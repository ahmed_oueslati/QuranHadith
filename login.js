const form = document.querySelector(".loginForm");

const login = async function (email, pass) {
  console.log(email, pass);
  const response = await fetch("https://apitest.khouaja.live/v1/user/login", {
    method: "POST",
    body: JSON.stringify({ email: email, password: pass }),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  const data = await response.json();
  console.log(data);
  console.log(data.token);
  localStorage.setItem("token", data.token);

  if (data.token === undefined) {
    alert("email or password is incorrect");
  } else {
    window.location.href = "home.html";
  }
};
form.addEventListener("submit", (e) => {
  e.preventDefault();
  const email = form.elements.email.value;
  const password = form.elements.password.value;
  login(email, password);
});
