const form = document.querySelector(".loginForm");

form.addEventListener("submit", (e) => {
  e.preventDefault();
  const email = form.elements.email.value;
  const password = form.elements.password.value;
  const name = form.elements.name.value;
  const phone = form.elements.phone.value;

  signUp(email, password, name, phone);
});

const signUp = async function (email, password, name, phone) {
  const response = await fetch("https://apitest.khouaja.live/v1/user/signup", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: email,
      name: name,
      phone: phone,
      password: password,
    }),
  });
  const data = await response.json();
  console.log(data);
  localStorage.setItem("token", data.token);
  window.location.href = "home.html";
};
