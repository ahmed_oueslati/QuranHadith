//variabls global
const token = localStorage.getItem("token");
const btnLogout = document.querySelector(".btnLogout");
const mainSection = document.querySelector(".mainSection");
const Section = document.querySelector(".section");
const userName = document.querySelector(".infoName");
const userEmail = document.querySelector(".infoEmail");
const openBook = document.querySelector(".openBook");
//

//check token
console.log(token);
if (!token) window.location = "login.html";
//
// log out
btnLogout.addEventListener("click", function () {
  localStorage.removeItem("token");
  window.location = "login.html";
});
//
// open book
openBook.addEventListener("click", function () {
  window.location = "home.html";
});
//
//get user logged in
const getUser = async function () {
  const response = await fetch("https://apitest.khouaja.live/v1/user/me", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  });
  const data = await response.json();
  userName.innerHTML = `<span class="infoName">${data.data.user.name}</span>`;
  userEmail.innerHTML = `<span class="infoEmail">${data.data.user.email}</span>`;
};
getUser();
//
// all surah sortin
const alphaSort = document.querySelector(".AlphabetSort");
const numberSort = document.querySelector(".numberSort");
const ayahSort = document.querySelector(".ayahSort");
//ayahSort
ayahSort.addEventListener("click", (e) => {
  e.preventDefault();
  const sortByAyah = async function () {
    const response = await fetch("https://apitest.khouaja.live/v1/quran/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await response.json();
    const allSurah = data.data;
    console.log(allSurah);
    const allSurahsorted = allSurah.sort(function (a, b) {
      if (a.ayahsNumber < b.ayahsNumber) return 1;
      if (a.ayahsNumber > b.ayahsNumber) return -1;
      return 0;
    });
    //
    const responseMarked = await fetch(
      "https://apitest.khouaja.live/v1/quran/bookmark",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const dataMarked = await responseMarked.json();
    console.log(dataMarked);

    afficheAll(allSurahsorted, dataMarked);
  };
  sortByAyah();
});
//
//numberSort
numberSort.addEventListener("click", (e) => {
  e.preventDefault();
  const sortByNumber = async function () {
    const response = await fetch("https://apitest.khouaja.live/v1/quran/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await response.json();
    const allSurah = data.data;
    console.log(allSurah);
    const allSurahsorted = allSurah.sort(function (a, b) {
      if (a.number < b.number) return -1;
      if (a.number > b.number) return 1;
      return 0;
    });
    //
    const responseMarked = await fetch(
      "https://apitest.khouaja.live/v1/quran/bookmark",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const dataMarked = await responseMarked.json();
    console.log(dataMarked);

    afficheAll(allSurahsorted, dataMarked);
  };

  sortByNumber();
});
//
//alphaSort
alphaSort.addEventListener("click", (e) => {
  e.preventDefault();
  const sortByAlpha = async function () {
    const response = await fetch("https://apitest.khouaja.live/v1/quran/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await response.json();
    const allSurah = data.data;
    console.log(allSurah);
    const allSurahsorted = allSurah.sort(function (a, b) {
      if (a.englishName < b.englishName) return -1;
      if (a.englishName > b.englishName) return 1;
      return 0;
    });
    //
    const responseMarked = await fetch(
      "https://apitest.khouaja.live/v1/quran/bookmark",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const dataMarked = await responseMarked.json();
    console.log(dataMarked);

    afficheAll(allSurahsorted, dataMarked);
  };

  sortByAlpha();
});
//
//
const affichBookmarked = function (bookmarked) {
  mainSection.innerHTML = "";
  bookmarked.forEach((element) => {
    //console.log(element._id);
    const html = `<div   data-number=${element.number}  class="Surah">
        <div  class="numberOfSurah">
        <span onclick="getSurah(${element.number})" class="number">${element.number}</span>
        <div class="fav" >
        <i class="fa-solid fa-heart liked"  onclick='postOnBook("${element._id}")' id="${element._id}"></i>
        </div>
        </div>
        <div class="nameOfSurah"  >
        <div class="nameEnglish">${element.englishName}</div>
        <div class="nameEnglish">${element.name}</div>
        </div>
        <div class="translatNameOfSurah">
        <p class="translate">${element.englishNameTranslation}</p>
        <span class="length">${element.ayahsNumber} ayah</span>
        </div>
        </div>
        `;
    mainSection.insertAdjacentHTML("beforeend", html);
  });
};

const afficheAll = function (data, bookmarked) {
  mainSection.innerHTML = "";
  console.log(bookmarked);
  const surahIds = bookmarked.data.map((item) => item.surahID);
  data.forEach((element) => {
    const html = `<div   data-number=${element.number}  class="Surah">
        <div  class="numberOfSurah">
        <span onclick="getSurah(${element.number})" class="number">${
      element.number
    }</span>
        <div class="fav" >
        <i class= "${
          surahIds.includes(element._id)
            ? "fa-solid fa-heart liked"
            : "fa-regular fa-heart"
        }" onclick='postOnBook("${element._id}")' id="${element._id}"></i>
        </div>
        </div>
        <div class="nameOfSurah"  >
        <div class="nameEnglish">${element.englishName}</div>
        <div class="nameEnglish">${element.name}</div>
        </div>
        <div class="translatNameOfSurah">
        <p class="translate">${element.englishNameTranslation}</p>
        <span class="length">${element.ayahsNumber} ayah</span>
        </div>
        </div>
        `;
    mainSection.insertAdjacentHTML("beforeend", html);
  });
};
//
// get all quran
const getQuran = async function () {
  const response = await fetch("https://apitest.khouaja.live/v1/quran/", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  });
  const data = await response.json();
  const allSurah = data.data;
  console.log(allSurah);
  //
  const responseMarked = await fetch(
    "https://apitest.khouaja.live/v1/quran/bookmark",
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    }
  );
  const dataMarked = await responseMarked.json();
  console.log(dataMarked);
  afficheAll(allSurah, dataMarked);
};

getQuran();
//
// get surah by number
const getSurah = async function (surahNumber) {
  const response = await fetch(
    `https://apitest.khouaja.live/v1/quran?surah=${surahNumber}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    }
  );
  const data = await response.json();
  console.log(data.data);
  afficheSurah(data.data); //surah
};
//
// affiche surah by number
const afficheSurah = function (surah) {
  let surahAyahs = "";
  surah.ayahs.forEach((ayah) => {
    surahAyahs += ayah.text + ayah.numberInSurah;
  });
  const html = `<div class="surahSection">${surahAyahs}</div>
  <button type="submit" id="playBtn" class="playSurah" value="${surah.number}">Play Audio </button>`;
  Section.innerHTML = "";
  Section.innerHTML = html;
  console.log(surahAyahs);
  const btnPlay = document.querySelector(".playSurah");
  btnPlay.addEventListener("click", function () {
    document.getElementById("playBtn").style.display = "none";
    console.log(btnPlay.value);
    const html = `<audio id="player" controls>
  <source src="" type="audio/mpeg">
</audio>`;
    Section.insertAdjacentHTML("beforeend", html);
    play(btnPlay.value);
  });
};

const play = function (Snumber) {
  let player = document.getElementById("player");
  player.src = `https://cdn.islamic.network/quran/audio-surah/128/ar.alafasy/${Snumber}.mp3`;
  player.play();
};
//
//searchForSurah();
const search = document.querySelector(".search");
const searchForSurah = async function () {
  const response = await fetch("https://apitest.khouaja.live/v1/quran/", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  });
  const data = await response.json();
  const dataSearch = data.data;
  const find = dataSearch.filter((item) =>
    item.englishName.toUpperCase().startsWith(search.value.toUpperCase())
  );
  console.log(find);

  const responseMarked = await fetch(
    "https://apitest.khouaja.live/v1/quran/bookmark",
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    }
  );
  const dataMarked = await responseMarked.json();
  afficheAll(find, dataMarked);
};
search.addEventListener("input", (e) => {
  e.preventDefault();
  searchForSurah();
});
//
//like icon
/* const like = () => {
  document.querySelectorAll(".like").forEach((icon) => {
    icon.addEventListener("click", (e) => {
      e.preventDefault();
      console.log(icon.id);
    });
  });
};
like(); */
//
// post surrah on bookmark by id
const postOnBook = async function (surahId) {
  let isLiked = document.getElementById(`${surahId}`);
  //isLiked.classList.toggle("liked");

  if (!isLiked.classList.contains("liked")) {
    isLiked.classList.add("fa-solid");
    isLiked.classList.remove("fa-regular");
    isLiked.classList.add("liked");
    const response = await fetch(
      "https://apitest.khouaja.live/v1/quran/bookmark",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          surahId: `${surahId}`,
        }),
      }
    );
    const data = await response.json();
    console.log(data);
  } else if (isLiked.classList.contains("liked")) {
    isLiked.classList.add("fa-regular");
    isLiked.classList.remove("fa-solid");
    console.log("deleted soon !");
    delet(surahId);
  }
};

// get all book mark surah by id
const bookMark = document.querySelector(".bookMark");
bookMark.addEventListener("click", (e) => {
  e.preventDefault();
  mainSection.innerHTML = "";
  const getAllBookMark = async function () {
    const response = await fetch(
      "https://apitest.khouaja.live/v1/quran/bookmark",
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    const dataMark = await response.json();
    console.log(dataMark);
    const surahIds = dataMark.data.map((item) => item.surahID);
    console.log(surahIds);
    const getQuran = async function () {
      const response = await fetch("https://apitest.khouaja.live/v1/quran/", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      const allSurah = await response.json();
      const newDataSurah = allSurah.data;
      console.log(newDataSurah);
      const favorits = surahIds.map((surahId) => {
        const fav = newDataSurah.filter((surrah) => surrah._id === surahId);
        return [...fav];
      });
      console.log(favorits);
      const eachFavorits = favorits.flat();
      console.log(eachFavorits);
      affichBookmarked(eachFavorits);
    };
    getQuran();
  };
  getAllBookMark();
});
//
// dellet surah by id
const delet = async function (id) {
  const response = await fetch(
    `https://apitest.khouaja.live/v1/quran/bookmark/${id}`,
    {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    }
  );
  const data = await response.json();
  console.log(data);
};
//
